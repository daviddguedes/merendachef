// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('app', ['ionic', 'app.controllers', 'app.routes', 'app.directives','app.services','ngStorage', 'ngCordova'])

  .run(function ($ionicPlatform, $rootScope, $state, $localStorage) {
    $ionicPlatform.ready(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);
      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }

      firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
          var id_user = user.uid;
          if ($localStorage.id_user) {
            delete $localStorage.id_user;
            $localStorage.id_user = id_user;
          } else {
            $localStorage.id_user = id_user;
          }

        } else {
          delete $localStorage.escola;
          if ($localStorage.id_user) {
            delete $localStorage.id_user;
          }
          $state.transitionTo("login");
        }
      });

    });

    $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
      if (toState.authenticate) {
        var user = firebase.auth().currentUser;
        if (!user) {
          $state.transitionTo("login");
          event.preventDefault();
        }
      }
    })

  })

