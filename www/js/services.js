angular.module('app.services', [])

    .service('MainService', function ($http) {
        this.getFeeds = function() {
            return $http({
                url: 'http://www.ajr.eti.br/mc/services/app/feed.json',
                method: "GET",
                headers: {
                    'Content-Type': 'application/json; charset=utf-8'
                }
            });
        }

        this.cadastrarAluno = function(aluno) {
            return $http.get("https://www.ajr.eti.br/mc/cadastro/index?nome="+aluno.nome+";email="+aluno.email+";senha="+aluno.senha+";id_escola="+aluno.id_escola);
        }

        this.loginAluno = function(login) {
            return $http.post("https://www.ajr.eti.br/mc/default/user/login", login);
        }

        this.listarEscolas = function() {
            return $http.get("https://www.ajr.eti.br/mc/services/app/escola.json");
        }


    })