angular.module('app.controllers', [])

    .controller('MainCtrl',
    function ($scope, $rootScope) {

    })

    .controller('feedCtrl', ['$scope', 'MainService', 'SERVER', function ($scope, MainService, SERVER) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            $scope.server = SERVER.imagemFeed;
            MainService.getFeeds().then(function (response) {
                $scope.feeds = response.data.content;
            }, function (error) {
                console.log(error);
            });
        });

    }])

    .controller('enviarFotoCtrl', ['$scope', '$stateParams', '$ionicModal', '$ionicPopup', '$state', '$cordovaCamera', '$q', '$rootScope',
        function ($scope, $stateParams, $ionicModal, $ionicPopup, $state, $cordovaCamera, $q, $rootScope) {

            $scope.datetime = new Date();
            $scope.haveMerenda = '';
            $scope.imagemCardapio = '';
            $scope.imagemMerenda = '';

            $ionicModal.fromTemplateUrl('modal-foto-cardapio.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.modalCardapio = modal;
            });

            $ionicModal.fromTemplateUrl('modal-foto-merenda.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function (modal) {
                $scope.modalMerenda = modal;
            });

            $scope.checkHasMerenda = function (op) {
                if (op == '') {
                    $ionicPopup.alert({
                        title: 'Alerta!',
                        template: 'Selecione ao menos uma opção'
                    });
                } else {
                    $scope.modalCardapio.show();
                }

            };

            $scope.tirarFotoCardapio = function () {
                $scope.tirarFotoDaCamera().then(function (imagemURL) {
                    $scope.imagemCardapio = imagemURL;
                    $scope.modalCardapio.hide();
                    $scope.modalMerenda.show();
                });
            }

            $scope.tirarFotoMerenda = function () {
                $scope.tirarFotoDaCamera().then(function (imagemURL) {
                    $scope.imagemMerenda = imagemURL;
                    $scope.modalMerenda.hide();
                    //TODO: enviar dados pro server, e no response ir pra tela de participação
                });
            }

            $scope.tirarFotoDaCamera = function () {
                var deferred = $q.defer();

                var options = {
                    quality: 50,
                    destinationType: Camera.DestinationType.DATA_URL,
                    sourceType: Camera.PictureSourceType.CAMERA,
                    allowEdit: true,
                    encodingType: Camera.EncodingType.JPEG,
                    targetWidth: 400,
                    targetHeight: 400,
                    popoverOptions: CameraPopoverOptions,
                    saveToPhotoAlbum: false,
                    correctOrientation: true
                };

                $cordovaCamera.getPicture(options).then(function (imageData) {
                    //var image = document.getElementById('myImage');
                    //image.src = "data:image/jpeg;base64," + imageData;
                    deferred.resolve(imageData);
                }, function (err) {
                    $scope.imagemCardapio = '';
                    $scope.imagemMerenda = '';
                    $scope.modalCardapio.hide();
                    $scope.modalMerenda.hide();
                });

                return deferred.promise;
            };

        }])

    .controller('confirmarMerendaCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams) {


        }])

    .controller('participaOCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams) {


        }])



    .controller('loginCtrl', ['$scope', '$http', 'MainService', '$state', '$localStorage', '$rootScope',
        function ($scope, $http, MainService, $state, $localStorage, $rootScope) {

            $scope.logar = function (login) {
                firebase.auth().signInWithEmailAndPassword(login.email, login.password).catch(function (error) {
                    var errorCode = error.code;
                    var errorMessage = error.message;
                }).then(function (response) {
                    var id_user = response.uid;

                    var ref = firebase.database().ref("/usuario/" + id_user);
                    var url = ref.toString() + ".json";
                    var usuarioObj = $http.get(url).then(function (res) {
                        $rootScope.userData = res;
                        $rootScope.nome_escola = $rootScope.userData.data.escola;
                    }, function (error) {
                        console.log(error);
                    });

                    if ($localStorage.id_user) {
                        delete $localStorage.id_user;
                        $localStorage.id_user = id_user;
                    } else {
                        $localStorage.id_user = id_user;
                    }

                    $state.go('tabsController.feed');
                });
                // MainService.loginAluno(login).then(function (response) {
                //     console.log(response);
                // }, function (error) {
                //     console.log(error);
                // });
            }

        }])


    .controller('cadastreSeCtrl', ['$scope', 'MainService', '$localStorage', '$state', function ($scope, MainService, $localStorage, $state) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            MainService.listarEscolas().then(function (response) {
                $scope.escolas = response.data.content;
                console.log($scope.escolas);
            }, function (error) {
                console.log(error);
            });
        });

        $scope.cadastrar = function (aluno) {
            var data = {
                "nome": aluno.nome,
                "email": aluno.email,
                "senha": aluno.password,
                "escola": aluno.escola.f_name
            }

            firebase.auth().createUserWithEmailAndPassword(data.email, data.senha).catch(function (error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                // ...
            }).then(function (response) {
                var id_user = response.uid;
                if ($localStorage.id_user) {
                    delete $localStorage.id_user;
                    $localStorage.id_user = id_user;
                } else {
                    $localStorage.id_user = id_user;
                }
                var ref = firebase.database().ref().child("usuario/" + id_user).set(data);
                if (ref) $state.go('tabsController.feed');
            });
        }
    }])

    .controller('pontuaOCtrl', ['$scope', '$rootScope', function ($scope, $rootScope) {



    }])

    .controller('confirmarMerendaCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams) {


        }])

    .controller('participaOCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams) {


        }])


    .controller('cadastreSeCtrl', ['$scope', 'MainService', '$localStorage', '$state', function ($scope, MainService, $localStorage, $state) {

        $scope.$on("$ionicView.beforeEnter", function (event, data) {
            MainService.listarEscolas().then(function (response) {
                $scope.escolas = response.data.content;
            }, function (error) {
                console.log(error);
            });
        });

        $scope.cadastrar = function (aluno) {
            var data = {
                "nome": aluno.nome,
                "email": aluno.email,
                "senha": aluno.password,
                "id_escola": aluno.escola.id
            }

            MainService.cadastrarAluno(data).then(function (response) {
                var id = response.data.id;
                if ($localStorage.id_user) {
                    delete $localStorage.id_user;
                    $localStorage.id_user = id;
                } else {
                    $localStorage.id_user = id;
                }
                $state.go('tabsController.feed');
            }, function (error) {
                console.log(error);
            });
        }

    }])


    .controller('inCioCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams) {


        }])

    .controller('consulteEscolaCtrl', ['$scope', '$stateParams', // The following is the constructor function for this page's controller. See https://docs.angularjs.org/guide/controller
        // You can include any angular dependencies as parameters for this function
        // TIP: Access Route Parameters for your page via $stateParams.parameterName
        function ($scope, $stateParams) {


        }])
