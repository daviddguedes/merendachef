angular.module('app.routes', [])

  .config(function ($stateProvider, $urlRouterProvider) {

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider

      .state('cadastreSe', {
        url: '/cadastro',
        templateUrl: 'templates/cadastreSe.html',
        controller: 'cadastreSeCtrl',
        authenticate: false
      })

      .state('login', {
        url: '/login',
        templateUrl: 'templates/login.html',
        controller: 'loginCtrl',
        authenticate: false
      })


      .state('tabsController.feed', {
        url: '/feed',
        views: {
          'tab1': {
            templateUrl: 'templates/feed.html',
            controller: 'feedCtrl'
          }
        },
        authenticate: true
      })

      .state('tabsController.enviarFoto', {
        url: '/enviar-foto',
        views: {
          'tab2': {
            templateUrl: 'templates/enviarFoto.html',
            controller: 'enviarFotoCtrl'
          }
        },
        authenticate: true
      })

      .state('tabsController.pontuaO', {
        url: '/pontuacao',
        views: {
          'tab3': {
            templateUrl: 'templates/pontuaO.html',
            controller: 'pontuaOCtrl'
          }
        },
        authenticate: true
      })

      .state('tabsController', {
        url: '/page1',
        templateUrl: 'templates/tabsController.html',
        abstract: true
      })

      .state('confirmarMerenda', {
        url: '/confirmar-merenda',
        templateUrl: 'templates/confirmarMerenda.html',
        controller: 'confirmarMerendaCtrl',
        authenticate: true
      })

      .state('participaO', {
        url: '/participacao',
        templateUrl: 'templates/participaO.html',
        controller: 'participaOCtrl',
        authenticate: true
      })

      .state('inCio', {
        url: '/inicio',
        templateUrl: 'templates/inCio.html',
        controller: 'inCioCtrl',
        authenticate: true
      })

      .state('consulteEscola', {
        url: '/consulte-escola',
        templateUrl: 'templates/consulteEscola.html',
        controller: 'consulteEscolaCtrl',
        authenticate: true
      })

    $urlRouterProvider.otherwise('/inicio')



  });